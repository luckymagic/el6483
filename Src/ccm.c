// this file will be placed in CCM RAM
// symbols in this file should be used frequently, like IRQ handlers

#include <stm32f3xx_hal.h>
#include "common.h"


// Distance sensors
TIM_HandleTypeDef DistanceTimerHandle;
__IO struct SensorsStatus Sensors = {.front = 0, .left = 0, .right = 0};

static __IO int32_t __attribute__((aligned(4))) elapsed = 0;
/**
 * \brief Total interval (60ms) recommanded in datasheet.
 */
static const int32_t ELAPSED_STEP = DISTANCE_TIMER_PERIOD * 1000000 / DISTANCE_TIMER_FREQUENCY;
static const float UltrasonicFactor = (float)1.7;
static void measure_distance();


// Motors
TIM_OC_InitTypeDef PWMTimerOCConfig;
TIM_HandleTypeDef PWMTimerHandle;
__IO struct WheelsStatus Moved = {.left = 0, .right = 0};
// um/ms
__IO struct WheelsStatus Speed = {.left = 0, .right = 0};
struct WheelsStatus move_command = {.left = 0, .right = 0};


static const MM MoveThreshold = 5;
/**
 * \brief Real movement since last move command
 */
static struct WheelsStatus last_move_command = {.left = 0, .right = 0};
// -100 ~ 100
static struct MotorOption pwm_ratio = {.left = 0, .right = 0};
static void set_pwm(uint32_t channel, bool start, uint32_t pulse);
static void motor_control();


static const int32_t MIN_LOOP_DURATION = 100;

void loop()
{
    // TODO: State Machine
    uint32_t last_measure = 0;
    move_command.left = 50;
    move_command.right = 50;
    HAL_Delay(1000);
    move(&move_command);
    while (1)
    {
        uint32_t start = HAL_GetTick();
        log("%u: \n", start);
        /*
        if (temp % 30 == 0)
        {
            log("straight\n");
            LastMoveCommand.left = 1;
            LastMoveCommand.right = 1;
            move(LastMoveCommand);
        }
        else if (temp % 30 == 10)
        {
            log("turn left\n");
            LastMoveCommand.left = 0;
            LastMoveCommand.right = 1;
            move(LastMoveCommand);
        }
        else if (temp % 30 == 20)
        {
            log("turn right\n");
            LastMoveCommand.left = 1;
            LastMoveCommand.right = 0;
            move(LastMoveCommand);
        }
        */

        motor_control();

        if (time_elapsed(last_measure) < DISTANCE_SENSOR_MIN_PERIOD)
        {
            HAL_Delay(STEP_SENSOR_MIN_PERIOD);
            motor_control();
        }
        last_measure = HAL_GetTick();
        measure_distance();
        log("Measurement takes %u ms", time_elapsed(last_measure));

        escape();

        // Get duty cycle
        //interval = TimeElapsed(start);
        log("Duty: %u ms\n", time_elapsed(start));
        HAL_Delay(MIN_LOOP_DURATION);
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    if (htim->Instance == DistanceTimerHandle.Instance)
    {
        elapsed++;
    }
}

static void switch_timer(uint8_t flag)
{
    if (flag)
    {
        elapsed = 0;
        if (HAL_TIM_Base_Start_IT(&DistanceTimerHandle) != HAL_OK)
        {
            /* Starting Error */
            Error_Handler();
        }
    }
    else
    {
        if (HAL_TIM_Base_Stop(&DistanceTimerHandle) != HAL_OK)
        {
            /* Starting Error */
            Error_Handler();
        }
    }
}

enum ultrasonic_status
{
    TRIGGERED,
    ECHOING,
    FINISHED,
    TIMEOUT
};

static inline void check_sensor_echo(enum ultrasonic_status* status, int32_t* timer, GPIO_TypeDef* port, uint16_t pin)
{
    switch (*status)
    {
    case TRIGGERED:
        if (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_SET)
        {
            *timer = elapsed;
            *status = ECHOING;
        }
        break;
    case ECHOING:
        if (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET)
        {
            *timer = elapsed - (*timer);
            *status = FINISHED;
        }
        break;
    case FINISHED:
    case TIMEOUT:
        break;
    }
}

static void measure_distance()
{
    log("  Measure Distance: ");

    //TODO: use seperate hardware timer and trigger (with filter)
    int32_t left_elapsed = 0, right_elapsed = 0, front_elapsed = 0;
    enum ultrasonic_status left_status = TRIGGERED, right_status = TRIGGERED, front_status = TRIGGERED;
    HAL_GPIO_WritePin(DISTANCE_LEFT_TRIG_PORT, DISTANCE_LEFT_TRIG_PIN, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DISTANCE_FRONT_TRIG_PORT, DISTANCE_FRONT_TRIG_PIN, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DISTANCE_RIGHT_TRIG_PORT, DISTANCE_RIGHT_TRIG_PIN, GPIO_PIN_SET);
    switch_timer(1);
    while (elapsed * ELAPSED_STEP < 10)
    {
    }
    switch_timer(0);
    HAL_GPIO_WritePin(DISTANCE_LEFT_TRIG_PORT, DISTANCE_LEFT_TRIG_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DISTANCE_FRONT_TRIG_PORT, DISTANCE_FRONT_TRIG_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DISTANCE_RIGHT_TRIG_PORT, DISTANCE_RIGHT_TRIG_PIN, GPIO_PIN_RESET);

    switch_timer(1);
    do
    {
        check_sensor_echo(&left_status, &left_elapsed, DISTANCE_LEFT_ECHO_PORT, DISTANCE_LEFT_ECHO_PIN);
        check_sensor_echo(&front_status, &front_elapsed, DISTANCE_FRONT_ECHO_PORT, DISTANCE_FRONT_ECHO_PIN);
        check_sensor_echo(&right_status, &right_elapsed, DISTANCE_RIGHT_ECHO_PORT, DISTANCE_RIGHT_ECHO_PIN);
        if (elapsed * ELAPSED_STEP > DISTANCE_SENSOR_MIN_PERIOD * 1000)
        {
            if (left_status != FINISHED) left_status = TIMEOUT;
            if (front_status != FINISHED) left_status = TIMEOUT;
            if (right_status != FINISHED) left_status = TIMEOUT;
            break;
        }
    }
    while (left_status != FINISHED || front_status != FINISHED || right_status != FINISHED);
    switch_timer(0);

    float distance;
    if (left_status == FINISHED)
    {
        distance = left_elapsed / 2 * UltrasonicFactor;
        Sensors.left = (MM)distance;
    }
    if (front_status == FINISHED)
    {
        distance = front_elapsed / 2 * UltrasonicFactor;
        Sensors.front = (MM)distance;
    }
    if (right_status == FINISHED)
    {
        distance = right_elapsed / 2 * UltrasonicFactor;
        Sensors.right = (MM)distance;
    }
    log("Left: %d mm, Front: %d mm, Right: %d mm\n", Sensors.left, Sensors.front, Sensors.right);
}

/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    static uint32_t last_left = 0;
    static uint32_t last_right = 0;
    uint32_t now = HAL_GetTick();
    int32_t since_last; 
    MM Movement;
    switch (GPIO_Pin)
    {
    case STEP_LEFT_PIN:
        since_last = (int32_t) interval(last_left, now);
        last_left = now;
        // Revolutions for wheel 1
        if (pwm_ratio.left < 0)
        {
            Movement = -WHEEL_STEP_MM;
            Speed.left = -WHEEL_STEP_UM / since_last;
        }
        else if (pwm_ratio.left > 0)
        {
            Movement = WHEEL_STEP_MM;
            Speed.left = WHEEL_STEP_UM / since_last;
        }
        else
        {
            // Try to infer direction if last move command for a wheel is 0 but the real movement is not zero
            // It may be caused by the movement of another wheel
            Movement = (pwm_ratio.right < 0) ? -WHEEL_STEP_MM : WHEEL_STEP_MM;
        }
        Moved.left += Movement;
        log("  Moved.left: %d\n", Moved.left);
        break;
    case STEP_RIGHT_PIN:
        since_last = (int32_t) interval(last_right, now);
        last_right = now;
        // Revolutions for wheel 2
        if (pwm_ratio.right < 0)
        {
            Movement = -WHEEL_STEP_MM;
            Speed.right = -WHEEL_STEP_UM / since_last;
        }
        else if (pwm_ratio.right > 0)
        {
            Movement = WHEEL_STEP_MM;
            Speed.right = WHEEL_STEP_UM / since_last;
        }
        else
        {
            Movement = (pwm_ratio.left < 0) ? -WHEEL_STEP_MM : WHEEL_STEP_MM;
        }
        Moved.right += Movement;
        log("  Moved.right: %d\n", Moved.right);
        break;
    default:
        break;
    }
}

inline uint32_t interval(uint32_t from, uint32_t to)
{
    return to - from;
}

inline uint32_t time_elapsed(uint32_t when)
{
    return interval(when, HAL_GetTick());
}

// Motor Control

static void set_pwm(uint32_t channel, bool start, uint32_t pulse)
{
    if (start)
    {
        /* Set the pulse value for channel */
        PWMTimerOCConfig.Pulse = pulse;
        if (HAL_TIM_PWM_ConfigChannel(&PWMTimerHandle, &PWMTimerOCConfig, channel) != HAL_OK)
        {
            /* Configuration Error */
            Error_Handler();
        }
        /* Start channel */
        if (HAL_TIM_PWM_Start(&PWMTimerHandle, channel) != HAL_OK)
        {
            /* PWM Generation Error */
            Error_Handler();
        }
    }
    else
    {
        if (HAL_TIM_PWM_Stop(&PWMTimerHandle, channel) != HAL_OK)
        {
            /* PWM Generation Error */
            Error_Handler();
        }
    }
}

struct WheelsStatus get_last_move_command()
{
    return last_move_command;
}


static void update_pwm()
{
    bool isClockwise;
    if (pwm_ratio.left == 0)
    {
        set_pwm(PWM_LEFT_CW_CHANNEL, false, 0);
        set_pwm(PWM_LEFT_CCW_CHANNEL, false, 0);
    }
    else
    {
        isClockwise = pwm_ratio.left > 0;
        set_pwm(PWM_LEFT_CW_CHANNEL, isClockwise, pulse_width((uint32_t)pwm_ratio.left));
        set_pwm(PWM_LEFT_CCW_CHANNEL, !isClockwise, pulse_width((uint32_t)pwm_ratio.left));
    }
    if (pwm_ratio.right == 0)
    {
        set_pwm(PWM_RIGHT_CW_CHANNEL, false, 0);
        set_pwm(PWM_RIGHT_CCW_CHANNEL, false, 0);
    }
    else
    {
        isClockwise = pwm_ratio.right > 0;
        set_pwm(PWM_RIGHT_CW_CHANNEL, isClockwise, pulse_width((uint32_t)pwm_ratio.right));
        set_pwm(PWM_RIGHT_CCW_CHANNEL, !isClockwise, pulse_width((uint32_t)pwm_ratio.right));
    }
}

void move(struct WheelsStatus* diff)
{
    last_move_command.left = diff->left;
    last_move_command.right = diff->right;
    Moved.left = 0;
    Moved.right = 0;
    motor_control();
}

void stop()
{
    pwm_ratio.left = 0;
    pwm_ratio.right = 0;
    update_pwm();
}

static void motor_control()
{
    //TODO: Use complementary signal for Movedotors
    //TODO: Use PID control

    //TODO: Handle the situation that the car moved beyond destination
    MM error_left = last_move_command.left - Moved.left;
    MM error_right = last_move_command.right - Moved.right;
    int32_t left_ratio = error_left * 100 / last_move_command.left;
    int32_t right_ratio = error_left * 100 / last_move_command.left;

    if (error_left < -MoveThreshold)
    {
        pwm_ratio.left = -left_ratio;
    }
    else if (error_left > MoveThreshold)
    {
        pwm_ratio.left = left_ratio;
    }
    else
    {
        pwm_ratio.left = 0;
    }

    if (error_right < -MoveThreshold)
    {
        pwm_ratio.right = -right_ratio;
    }
    else if (error_right > MoveThreshold)
    {
        pwm_ratio.right = right_ratio;
    }
    else
    {
        pwm_ratio.right = 0;
    }
    update_pwm();
}


// IRQ Handlers
void SysTick_Handler(void)
{
    HAL_IncTick();
    HAL_SYSTICK_IRQHandler();
}

void EXTI2_TSC_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

void TIM4_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&DistanceTimerHandle);
}
