/**
  ******************************************************************************
  * @file    TIM/TIM_PWMOutput/Src/main.c
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    01-July-2016
  * @brief   This sample code shows how to use STM32F3xx TIM HAL API to generate
  *          4 signals in PWM.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stm32f3xx_hal.h>
#include <stm32f3_discovery.h>
#include "main.h"
#include "common.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Timer handler declaration */
/* Timer Output Compare Configuration Structure declaration */

/* Counter Prescaler value */
static uint32_t uhPrescalerValue = 0;

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void PWM_Config(void);
static void Distance_Config(void);
static void Revolution_Config(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
    HAL_Init();
    /* Configure the system clock to 72 MHz */
    SystemClock_Config();
    
    GPIO_CLK_ENABLE();

    

    PWM_Config();
    Distance_Config();
    Revolution_Config();

    loop();
}

static void PWM_Config(void)
{
    /* Compute the prescaler value to have TIM3 counter clock equal to 24000000 Hz */
    uhPrescalerValue = (uint32_t)(SystemCoreClock / 2000000) - 1;

    /*##-1- Configure the TIM peripheral #######################################*/
    /* -----------------------------------------------------------------------
    TIM3 Configuration: generate 4 PWM signals with 4 different duty cycles.
  
  In this example TIM3 input clock (TIM3CLK) is set to 2 * APB1 clock (PCLK1), 
      since APB1 prescaler is different from 1.   
        TIM3CLK = 2 * PCLK1  
        PCLK1 = HCLK / 2 
        => TIM3CLK = HCLK = SystemCoreClock
            
      To get TIM3 counter clock at 24 MHz, the prescaler is computed as follows:
         Prescaler = (TIM3CLK / TIM3 counter clock) - 1
         Prescaler = (SystemCoreClock /24 MHz) - 1
                                                
      To get TIM3 output clock at 36 KHz, the period (ARR)) is computed as follows:
         ARR = (TIM3 counter clock / TIM3 output clock) - 1
             = 665
                    
      TIM3 Channel1 duty cycle = (TIM3_CCR1/ TIM3_ARR)* 100 = 50%
      TIM3 Channel2 duty cycle = (TIM3_CCR2/ TIM3_ARR)* 100 = 37.5%
      TIM3 Channel3 duty cycle = (TIM3_CCR3/ TIM3_ARR)* 100 = 25%
      TIM3 Channel4 duty cycle = (TIM3_CCR4/ TIM3_ARR)* 100 = 12.5%
  
      Note:
       SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f3xx.c file.
       Each time the core clock (HCLK) changes, user had to update SystemCoreClock
       variable value. Otherwise, any configuration based on this variable will be incorrect.
       This variable is updated in three ways:
        1) by calling CMSIS function SystemCoreClockUpdate()
        2) by calling HAL API function HAL_RCC_GetSysClockFreq()
        3) each time HAL_RCC_ClockConfig() is called to configure the system clock frequency
    ----------------------------------------------------------------------- */

    /* Initialize TIMx peripheral as follows:
         + Prescaler = (SystemCoreClock / 24000000) - 1
         + Period = (665 - 1)
         + ClockDivision = 0
         + Counter direction = Up
    */
    PWMTimerHandle.Instance = TIMx;

    PWMTimerHandle.Init.Prescaler = uhPrescalerValue;
    PWMTimerHandle.Init.Period = PWM_PERIOD;
    PWMTimerHandle.Init.ClockDivision = 0;
    PWMTimerHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
    PWMTimerHandle.Init.RepetitionCounter = 0;
    if (HAL_TIM_PWM_Init(&PWMTimerHandle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }

    /*##-2- Configure the PWM channels #########################################*/
    /* Common configuration for all channels */
    PWMTimerOCConfig.OCMode = TIM_OCMODE_PWM1;
    PWMTimerOCConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    PWMTimerOCConfig.OCNPolarity = TIM_OCNPOLARITY_HIGH;
    PWMTimerOCConfig.OCFastMode = TIM_OCFAST_DISABLE;
    PWMTimerOCConfig.OCIdleState = TIM_OCIDLESTATE_RESET;
    PWMTimerOCConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
    /* Turn LED3 on */
    BSP_LED_On(LED_RED);
    while (1)
    {
    }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 72000000
  *            HCLK(Hz)                       = 72000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV                     = 1
  *            PLLMUL                         = RCC_PLL_MUL9 (9)
  *            Flash Latency(WS)              = 2
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        /* Initialization Error */
        while (1);
    }

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
       clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
        /* Initialization Error */
        while (1);
    }
}

/* Init timer, GPIO for measuring distance */
static void Distance_Config(void)
{
    DistanceTimerHandle.Instance = DISTANCE_TIMER;
    DistanceTimerHandle.Init.Prescaler = (uint32_t) (SystemCoreClock / DISTANCE_TIMER_FREQUENCY) - 1;
    DistanceTimerHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
    //10us
    DistanceTimerHandle.Init.Period = DISTANCE_TIMER_PERIOD - 1;
    DistanceTimerHandle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&DistanceTimerHandle) != HAL_OK)
    {
        Error_Handler();
    }

    /* Configure User Button, connected to PE6 IOs in External Interrupt Mode with Rising edge trigger detection. */
    GPIO_InitTypeDef distance_echo;
    distance_echo.Mode = GPIO_MODE_INPUT;
    distance_echo.Pull = GPIO_PULLDOWN;
    distance_echo.Speed = GPIO_SPEED_FREQ_HIGH;

    //distance_echo.Pin = DISTANCE_LEFT_ECHO_PIN;
    //HAL_GPIO_Init(DISTANCE_LEFT_ECHO_PORT, &distance_echo);
    distance_echo.Pin = DISTANCE_FRONT_ECHO_PIN;
    HAL_GPIO_Init(DISTANCE_FRONT_ECHO_PORT, &distance_echo);
    distance_echo.Pin = DISTANCE_RIGHT_ECHO_PIN;
    HAL_GPIO_Init(DISTANCE_RIGHT_ECHO_PORT, &distance_echo);

    GPIO_InitTypeDef distance_trig;
    distance_trig.Mode = GPIO_MODE_OUTPUT_PP;
    distance_trig.Speed = GPIO_SPEED_HIGH;
    distance_trig.Pull = GPIO_PULLDOWN;

    distance_trig.Pin = DISTANCE_LEFT_TRIG_PIN;
    HAL_GPIO_Init(DISTANCE_LEFT_TRIG_PORT, &distance_trig);
    HAL_GPIO_WritePin(DISTANCE_LEFT_TRIG_PORT, DISTANCE_LEFT_TRIG_PIN, GPIO_PIN_RESET);
    distance_trig.Pin = DISTANCE_FRONT_TRIG_PIN;
    HAL_GPIO_Init(DISTANCE_FRONT_TRIG_PORT, &distance_trig);
    HAL_GPIO_WritePin(DISTANCE_FRONT_TRIG_PORT, DISTANCE_FRONT_TRIG_PIN, GPIO_PIN_RESET);
    distance_trig.Pin = DISTANCE_RIGHT_TRIG_PIN;
    HAL_GPIO_Init(DISTANCE_RIGHT_TRIG_PORT, &distance_trig);
    HAL_GPIO_WritePin(DISTANCE_RIGHT_TRIG_PORT, DISTANCE_RIGHT_TRIG_PIN, GPIO_PIN_RESET);
}

/* Init GPIO EXTI for measuring revolution of wheels */
static void Revolution_Config(void)
{
    GPIO_InitTypeDef revolution_exti;
    revolution_exti.Pin = STEP_LEFT_PIN;
    revolution_exti.Mode = GPIO_MODE_IT_FALLING;
    revolution_exti.Pull = GPIO_PULLDOWN;
    revolution_exti.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(STEP_LEFT_PORT, &revolution_exti);
    HAL_NVIC_SetPriority(EXTI2_TSC_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(EXTI2_TSC_IRQn);
}


void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim)
{
    /*##-1- Enable peripherals and GPIO Clocks #################################*/
    /* TIMx Peripheral clock enable */
    DISTANCE_TIMER_CLK_ENABLE();

    /*##-2- Configure the NVIC for TIMx ########################################*/
    /* Set Interrupt Group Priority */
    HAL_NVIC_SetPriority(DISTANCE_TIMER_IRQn, 4, 0);

    /* Enable the TIMx global Interrupt */
    HAL_NVIC_EnableIRQ(DISTANCE_TIMER_IRQn);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim)
{
  GPIO_InitTypeDef   GPIO_InitStruct;
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIMx_CLK_ENABLE();

  /* Enable all GPIO Channels Clock requested */
  TIMx_CHANNEL_GPIO_PORT();

  /* Configure PC.06 (TIM3_Channel1), PC.07 (TIM3_Channel2), PC.08 (TIM3_Channel3),
     PC.09 (TIM3_Channel4) in output, push-pull, alternate function mode
  */
  /* Common configuration for all channels */
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

  GPIO_InitStruct.Alternate = TIMx_GPIO_AF_CHANNEL1;
  GPIO_InitStruct.Pin = TIMx_GPIO_PIN_CHANNEL1;
  HAL_GPIO_Init(TIMx_GPIO_PORT_CHANNEL1, &GPIO_InitStruct);

  GPIO_InitStruct.Alternate = TIMx_GPIO_AF_CHANNEL2;
  GPIO_InitStruct.Pin = TIMx_GPIO_PIN_CHANNEL2;
  HAL_GPIO_Init(TIMx_GPIO_PORT_CHANNEL2, &GPIO_InitStruct);

  GPIO_InitStruct.Alternate = TIMx_GPIO_AF_CHANNEL3;
  GPIO_InitStruct.Pin = TIMx_GPIO_PIN_CHANNEL3;
  HAL_GPIO_Init(TIMx_GPIO_PORT_CHANNEL3, &GPIO_InitStruct);

  GPIO_InitStruct.Alternate = TIMx_GPIO_AF_CHANNEL4;
  GPIO_InitStruct.Pin = TIMx_GPIO_PIN_CHANNEL4;
  HAL_GPIO_Init(TIMx_GPIO_PORT_CHANNEL4, &GPIO_InitStruct);
}

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
