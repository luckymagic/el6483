#pragma once

#include <sys/_stdint.h>
#include <stddef.h>
#include <stdbool.h>

// <Misc

#ifdef DEBUG
#define log(...) printf(__VA_ARGS__)
#else
#define log(...)
#endif

void Error_Handler(void);

/**
 * \brief Calculate the interval from `when` to now
 * \param when Time to diff
 * \return Milliseconds
 */
uint32_t time_elapsed(uint32_t when);
uint32_t interval(uint32_t from, uint32_t to);

void loop(void) __attribute__((noreturn));

typedef int32_t MM;

// Misc/>


// <Hardware

// <Step sensors

// 90mm / 100 ms
static const int32_t STEP_SENSOR_MIN_PERIOD = 10;
/* Raw sensor output: step by 1/20 of a circle
 * So we need to add/subtract `left` and `right` by (d * pi / 20) = 65*3.14/20 = 10.2101761242 ~= 10
 */
static const int32_t WHEEL_STEP_MM = 10;
static const int32_t WHEEL_STEP_UM = 10210;

#define STEP_LEFT_PIN GPIO_PIN_2
#define STEP_LEFT_PORT GPIOA
#define STEP_RIGHT_PIN GPIO_PIN_3
#define STEP_RIGHT_PORT GPIOA

inline void GPIO_CLK_ENABLE()
{
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
}

struct WheelsStatus
{
    MM left;
    MM right;
} __attribute__ ((aligned (4)));

struct WheelsStatus get_last_move_command(void);
extern struct WheelsStatus move_command;
extern __IO struct WheelsStatus Moved;

// Step sensors/>

// <Distance sensors

static const MM MIN_SENSOR_DISTANCE = 30;
static const MM MAX_SENSOR_DISTANCE = 4000;

#define DISTANCE_TIMER_FREQUENCY 24000000
#define DISTANCE_TIMER_PERIOD 120
static const int32_t DISTANCE_SENSOR_MIN_PERIOD = 60;

// #0 (C14)
#define DISTANCE_RIGHT_ECHO_PIN GPIO_PIN_11
#define DISTANCE_RIGHT_ECHO_PORT GPIOE
#define DISTANCE_RIGHT_TRIG_PIN GPIO_PIN_10
#define DISTANCE_RIGHT_TRIG_PORT GPIOE

// #1 (C15)
#define DISTANCE_FRONT_ECHO_PIN GPIO_PIN_6
#define DISTANCE_FRONT_ECHO_PORT GPIOF
//#define DISTANCE_FRONT_TRIG_PIN GPIO_PIN_0
//#define DISTANCE_FRONT_TRIG_PORT GPIOB
#define DISTANCE_FRONT_TRIG_PIN GPIO_PIN_12
#define DISTANCE_FRONT_TRIG_PORT GPIOA

// #2 (C16)
#define DISTANCE_LEFT_ECHO_PIN GPIO_PIN_13
#define DISTANCE_LEFT_ECHO_PORT GPIOA
//#define DISTANCE_LEFT_TRIG_PIN GPIO_PIN_0
//#define DISTANCE_LEFT_TRIG_PORT GPIOD
#define DISTANCE_LEFT_TRIG_PIN GPIO_PIN_11
#define DISTANCE_LEFT_TRIG_PORT GPIOA

#define DISTANCE_TIMER TIM4
#define DISTANCE_TIMER_IRQn TIM4_IRQn
inline void DISTANCE_TIMER_CLK_ENABLE()
{
    __HAL_RCC_TIM4_CLK_ENABLE();
}

struct SensorsStatus
{
    MM left;
    MM right;
    MM front;
} __attribute__ ((aligned (4)));

extern TIM_HandleTypeDef DistanceTimerHandle;
extern const int32_t DISTANCE_SENSOR_MIN_PERIOD;
extern __IO struct SensorsStatus Sensors;

// Distance sensors/>

// <Motors

#define PWM_PERIOD (uint32_t)(665 - 1) /* Period Value  */

inline uint32_t pulse_width(uint32_t percentage)
{
    if (percentage >= 100) return PWM_PERIOD;
    if (percentage == 0) return 0;
    return PWM_PERIOD * percentage / 100;
}

/*
Motor 1

PD6	PD7	Motor State
L	L	Stopped
L	H	Rotate CW
H	L	Rotate CCW
H	H	Stopped

Motor 2

PD3	PD4	Motor State
L	L	Stopped
L	H	Rotate CW
H	L	Rotate CCW
H	H	Stopped
*/

#define PWM_LEFT_CW_CHANNEL TIM_CHANNEL_3
#define PWM_LEFT_CCW_CHANNEL TIM_CHANNEL_4
#define PWM_RIGHT_CW_CHANNEL TIM_CHANNEL_2
#define PWM_RIGHT_CCW_CHANNEL TIM_CHANNEL_1

struct MotorOption
{
    int32_t left;
    int32_t right;
} __attribute__ ((aligned (4)));

extern TIM_HandleTypeDef PWMTimerHandle;
extern TIM_OC_InitTypeDef PWMTimerOCConfig;

// Motors/>

// Hardware/>


// <Software

/**
 * \brief Try to add diff.left to current wheels.left and diff.right to current wheels.right.
 * \param diff Move command
 * It should use PWM and other GPIO pins and update the CurrentMotorOption.
 */
void move(struct WheelsStatus* diff);
void stop(void);
void escape(void);

// Software/>
