#include <stm32f3xx_hal.h>
#include <stm32f3_discovery.h>
#include "main.h"
#include "common.h"

int route = 0;//判断是否在原地转圈
int done = 0;//判断start是否完成
//String route = ""; //第一次走迷宫时记录每个路口行走标志：直行S，左转L，右转R，掉头B
//boolean done = false;    //第一次迷宫是否走完标志。fasle：未走完  true：走完
int Sl,Sr,Sf; // 保存前中后3个传感器的状态


void setup() //初始化
{

}

void loop() //主循环左手法则
{
	read_data();
	if(done == 0)
	{
		start();
	}
	if(Sl >= 3000 && Sr >= 3000)
	{	
		one_step();
		escape();
	}
	else if(Sl >= 100)
	{
		route += 1; //记录转向标志，左转加一，右转减一
		one_step();
		turn_left();
	}
	else if (Sf >= 100)
	{
		straight();
		read_data();
	}
	else if (Sr >= 100)
	{
		route -= 1;
		turn_right();
	}
	else
	{
		route = 0;
		turn_back();
	}
	routing();
}

//当route大于4，,说明在原地转圈,重新开始
void routing() 
{
 if (route > 4)
 {
 	done = 0;
 	route = 0;
 }
}

//找墙右转，保证左面有墙
void start() 
{
	read_data();
		do
		{
			straight();
			read_data();
		}while(Sf >= 100 && (Sl <= 3000 && Sr <= 3000));//在前方大于10cm并且在迷宫内时前进
		done = 1;     //start完成标志
		escape();
		turn_right();
}

//停止
void stop()
{
	move(0, 0);
}

void turn_left()
{
	do
	{
		read_data();
		move(0,10);//左轮不转，右轮转
	}while(Sl <= 40);//参数不太确定，需要实际测量//while (Sf <= 250)；
}

void turn_right()
{
	do
	{
		read_data();
		move(10,0);//右轮不转，左轮转
	}while (Sl >= 40);//参数不太确定，需要实际测量
}
//直行并调整方向
void straight()
{
	if (Sl <= 20)
	{
		move(15, 10);//左轮转速为右轮1.5倍
	}
	if (Sr <= 20)
	{
		move(10, 15);
	}
	else
	{
		move(10, 10);
	}
}

void turn_back()
{
	do
	{
		read_data();
		move(10.-10);//原地转弯，左右轮相反转向
	}while (Sf >= 250);
}

//小车控制,“+ -”方向(+:逆时针转；-:顺时针转)，“l r”代表的是左右轮移动距离或者速度
void move(int l,int r)   
{
  move_command.left = l;
  move_command.right = r;
}

//当左右距离无穷大时，判断走出迷宫
void escape(struct SensorsStatus)
{
	do 
	{
        stop();       
        read_data();
    }while (Sl >= 3000) && (Sr >= 3000);
}

 //到达左转路口时,前进一小步,防止碰撞
void one_step()
{         
 move(30,30);//走三厘米
}

//读取传感器数值
void read_data(struct SensorsStatus)
{
  Sl = SensorsStatus.left;
  Sr = SensorsStatus.right;
  Sf = SensorsStatus.front;
}

